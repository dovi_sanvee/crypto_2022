from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.x509 import CertificateBuilder
from cryptography.x509.oid import NameOID
import datetime
import uuid

from generator import generator

# génération de la private key de CA root
private_key = rsa.generate_private_key(
    public_exponent=65537,
    key_size=2048,
    backend=default_backend()
)
# génération de la public  key de CA root
public_key = private_key.public_key()


def sign_certificate(cert: CertificateBuilder, hash_algo: hashes):
    certificate_final = cert.sign(
        private_key=private_key, algorithm=hash_algo,
        backend=default_backend()
    )
    with open("ca.crt", "wb") as f:
        f.write(certificate_final.public_bytes(
            encoding=serialization.Encoding.PEM,
        ))
    print(isinstance(certificate_final, x509.Certificate))


def authority_root():

    unsigned_ca_certificate = generator.generate_cert("ca","esiea","groupe-esiea",public_key)
    sign_certificate(unsigned_ca_certificate,hashes.SHA256)
    with open("ca.key", "wb") as f:
        f.write(private_key.private_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PrivateFormat.TraditionalOpenSSL,
            encryption_algorithm=serialization.BestAvailableEncryption(b"openstack-ansible")
        ))


