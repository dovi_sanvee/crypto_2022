from cryptography.hazmat.primitives import hashes

from register_CA import ae

if __name__ == '__main__':
    commonName = input("veuillez saisir votre  nom ? : ")
    organizationName = input("veuillez saisir  le nom de l'organisation du certification? : ")
    organizationUnitName = input("veuillez saisir le nom de l'unité organisation ? ")
    hash_algo = int(input("Veuillez saisir quel algo de chiffrement et se signature a utiliser :"
                     "\nTaper 0 pour SHA256\n"
                     "Taper 1 pour SHA3_384\n"
                     "Taper 2 pour SHA224\n"
                     "Taper 3 pour SHA3_512\n"
                     "Taper 4 pour SHA512_224\n"
                     "Taper 5 pour SHA512_256\n"
                     "Taper 6 pour BLAKE2b\n"
                     "Taper 7 pour BLAKE2s\n"
                     "Taper 8 pour SM3\n"
                     "Taper 9 pour SHA1\n"
                     "Taper 10 pour SHA3_224\n"
                     "Taper 11 pour SHA3_256\n"
                     "Taper 12 pour SHA384\n"
                     "Taper 13 pour SHAKE128\n"
                     "Taper 14 pour MD5\n"
                     "Taper 15 pour SHAKE256\n"))

    match hash_algo:
        case 0:
            h = hashes.SHA256()
        case 1:
            h = hashes.SHA3_384()
        case 2:
            h = hashes.SHA224()
        case 3:
            h = hashes.SHA3_512()
        case 4:
            h = hashes.SHA512_224()
        case 5:
            h = hashes.SHA512_256()
        case 6:
            h = hashes.BLAKE2b()
        case 7:
            h = hashes.BLAKE2s()
        case 8:
            h = hashes.SM3()
        case 9:
            h = hashes.SHA1()
        case 10:
            h = hashes.SHA3_224()
        case 11:
            h = hashes.SHA3_256()
        case 12:
            h = hashes.SHA384()
        case 13:
            h = hashes.SHAKE128()
        case 14:
            h = hashes.MD5()
        case 15:
            h = hashes.SHAKE256()
        case _:
            h = hashes.SHA256()

    ae.register_authority()

